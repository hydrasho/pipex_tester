//valac main.vala --pkg=posix --pkg=gio-2.0 --vapidir=./

const int SEGFAULT = -699999;
const int LEAK = -42000;
public string pipex_emp;

bool compare_output(string file) throws Error {
	string s1;
	string s2;
	
	FileUtils.get_contents(file + "A", out s1);
	FileUtils.get_contents(file + "B", out s2);

	if (s1 == s2)
		return true;
	return false;
}

int bash_test_cmd(string infile, string cmd1, string cmd2, string outfile) throws Error
{
	string null_file;
	int wait_status;

	Process.spawn_command_line_sync(@"/bin/bash -c '< $infile $cmd1 | $cmd2 > $outfile'", out null_file, out null_file, out wait_status);
	return wait_status;
}

bool check_valgrind_leak (string errput) {
	foreach (var i in errput.split ("\n")) {
		if ("total heap usage:" in i) {
			unowned string str = i.offset(i.index_of("total"));
			int alloc;
			int free;
			int bytes;
			str.scanf("total heap usage: %d allocs, %d frees, %d bytes allocated", out alloc, out free, out bytes);
			if (alloc != free) {
				print(@"\033[31m[LEAK] $alloc Alloc and $free Free (%bytes) Bytes\n\033[0m");
				return false;
			}
		}
	}
	return true;
}

int pipex_test_cmd(string infile, string cmd1, string cmd2, string outfile) throws Error
{
	string output;
	string errput;
	int wait_status;

	Process.spawn_command_line_sync(@"valgrind $pipex_emp '$infile' '$cmd1' '$cmd2' '$outfile'", out output, out errput, out wait_status);
	if (Process.if_signaled(wait_status)) {
		if (Process.core_dump(wait_status))
			return SEGFAULT;
		print("[Error] pipex catch signal: (%s)\n", strsignal(Process.term_sig(wait_status)));
	}
	if (check_valgrind_leak(errput) == false)
		return LEAK;
	return wait_status;
}

bool test(string infile, string cmd1, string cmd2, string outfile)
{
	try {
		var status_p = pipex_test_cmd(infile, cmd1, cmd2, outfile + "A");
		var status_b = bash_test_cmd(infile, cmd1, cmd2, outfile + "B");

		if (status_p == SEGFAULT) {
			print(@"\033[31m[KO] [SEGFAULT]\n $infile $cmd1 $cmd2 $outfile\033[0m");
			return false;
		}
		if (status_b != status_p) {
			print(@"\033[31m[KO] Bad return Status\n\t ./pipex '$infile' '$cmd1' '$cmd2' '$outfile'\033[0m\n");
			return false;
		}
		if (compare_output(outfile) == false) {
			print(@"\033[31m[KO]: ./pipex '$infile' '$cmd1' '$cmd2' '$outfile'\033[0m\n");
			return false;
		}
		print("\033[32m[OK]\033[0m\n");
	} catch (Error e) {
		printerr(e.message);
	}
	return true;
}

string create_file_tmp_chmod0 () {
	try {
		string content;
		FileUtils.open_tmp("abcXXXXXX", out content);
		FileUtils.chmod(content, 0000);
		return content;
	}
	catch (Error e) {
		error(e.message);
	}
}

int	main()
{
	if (FileUtils.test("./pipex", FileTest.EXISTS)) {
		pipex_emp = "./pipex";
	}
	else if (FileUtils.test("../pipex", FileTest.EXISTS)) {
		pipex_emp = "../pipex";
	}
	else {	
		print("\033[96;1m [INFO] \033[0m pipex not found \n");
		return -1;
	}

	FileUtils.chmod("pipex", 0755);
	test("/dev/full", "cat -e", "head -c50", "Hola");
	test("/dev/full", "cat -e", "head -c50", "./Hola");
	test("/dev/zero", "cat -e", "head -c50", "Hola");
	test("ingalls", "cat -e", "grep -o un", "Hola");
	test("ingalls", "/bin/cat -e", "grep -o un", "Hola");
	test("ingalls", "cat -e", "/bin/grep -o un", "Hola");
	test("ingalls", "/bin/cat -e", "/bin/grep -o un", "Hola");
	test("ingalls", "sdjfof", "/bin/grep -o un", "Hola");
	test("ingalls", "sdjfof", "ls", "Hola");
	test("ingalls", "sdjfof", "/bin/ls", "Hola");
	test("ingalls", "ls", "nexistpas", "Hola");
	test("dorothe", "cat -e", "grep -o yeah", "././././././Hola");
	test("dorothe", "/bin/cat -e", "grep -o yeah", "././././././Hola");
	test("dorothe", "printenv", "grep -o LANGUAGE", "./Hola");
	test(create_file_tmp_chmod0(), "cat", "cat -e", "./Hola");
	return (0);
}
